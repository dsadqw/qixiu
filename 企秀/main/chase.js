require({
    paths : {
        jquery : '../unis/jquery-1.8.3.min',
        conmodityLists : '../js/conmodityLists'
    }
});
require(['jquery','conmodityLists'],function(jquery,conmodityLists){
    var opt = {
        Doms : function(){
            this.doms = {
                aside : $('.aside'),
                navfilst : $('.asideShow')
            }
        },
        init : function(){
            var _self = this;
            _self.Doms();
            _self.addEvent();
        },
        addEvent : function(){
            var _self = this;
            var _t = _self.doms;
            conmodityLists.opt.show(_t.navfilst,_t.aside);
        }
    };
    opt.init();


});